<?php
namespace BackendTest\V1\Rest\EquipmentType;

use BackendTest\lib\BaseObjectTest;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;


class EquipmentTypeApiTest extends BaseObjectTest
{
    protected function setUp()
    {
        $this->setPath('/backend/equipment-type');
        parent::setUp();
    }
//inputs
    public function inputs_SingleItem()
    {
        return array([array(
            'id' => 11)]);
    }
    
    public function inputs_Filter()
    {
        return array([array()]);
    }
    public function testCreate()
    {
        return array(
            'title' => 'TestEqipmentUpdate'
        );
    }
    public function inputs_UpdateElement()
    {
        return array([array("field" => array("title"), "val" => array('testEqupUpdate'))]);
    }

    public function inputs_Sort()
    {
        return array([array()]);
    }

    public function input_OverviewEmbedded()
    {
        return array(['overview', 'Solid', '1']);
    }

    public function illegalInput_SingleItem()
    {
        return array(['&', '’%20or%20’1’=’1', '1=1']);
    }

    public function illegalInput_CreateElement()
    {
        return array([array("field" => array("title1"), "val" => array('testUpdateColor'))]);
    }

    public function illegalInput_UpdateElement()
    {
        return array(['16', array(
            'title1' => 'illtestnew')], ['illegal', array(
            'title' => 'illtestnew')], ['100', array(
            'title' => 'illtestnew')], ['83', array(
            'title' => 89)]);
    }

    public function illegalInput_DeleteElement()
    {
        return array(['dfsd', '’%20or%20’1’=’1', '1=1']);
    }

    public function illegalInput_Sort()
    {
      
        return array([array()]);

    }

    public function illegalInput_Filter()
    {
        return array([array()]);

    }

    public function illegalInput_QueryParameter()
    {
        return array(['illegalparam', 'illegalparam=1']);
    }

}

