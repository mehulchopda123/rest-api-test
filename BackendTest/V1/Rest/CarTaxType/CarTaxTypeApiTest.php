<?php
namespace BackendTest\V1\Rest\ColorType;

use BackendTest\lib\BaseObjectTest;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;


class CarTaxTypeApiTest extends BaseObjectTest
{
    protected function setUp()
    {
        $this->setPath('/backend/car-tax-type');
        parent::setUp();
    }
//inputs
    public function inputs_SingleItem()
    {
        return array([array(
            'id' => 2)]);
    }
    
    public function inputs_Filter()
    {
        return array(["field" => (array("id" => 1))],
            ["field" => (array("title" => "PK"))],
            ["field" => (array("id"=>2,"title"=>"LK"))]
        );
        

       
    }
    public function testCreate()
    {
        return array(
            'title' => 'TestCarTaxUpdate'
        );
    }
    public function inputs_UpdateElement()
    {
        return array([array("field" => array("title"), "val" => array('testTaxCar'))]);
    }

    public function inputs_Sort()
    {
        return array("sort" => [array(
            "field" => "id", "dir" => "asc", "type" => "num")], [array(
            "field" => "id", "dir" => "desc", "type" => "num")], [array(
            "field" => "title", "dir" => "asc", "type" => "string")], [array(
            "field" => "title", "dir" => "desc", "type" => "string")]);
    }

    public function input_OverviewEmbedded()
    {
        return array(['overview', 'Solid', '1']);
    }

    public function illegalInput_SingleItem()
    {
        return array(['&', '’%20or%20’1’=’1', '1=1']);
    }

    public function illegalInput_CreateElement()
    {
        return array([array("field" => array("title1"), "val" => array('testUpdateColor'))]);
    }

    public function illegalInput_UpdateElement()
    {
        return array(['16', array(
            'title1' => 'illtestnew')], ['illegal', array(
            'title' => 'illtestnew')], ['100', array(
            'title' => 'illtestnew')], ['83', array(
            'title' => 89)]);
    }

    public function illegalInput_DeleteElement()
    {
        return array(['dfsd', '’%20or%20’1’=’1', '1=1']);
    }

    public function illegalInput_Sort()
    {
        return array([array(
            'sort' => "id=desc1")], [array(
            'sort' => "name=desc1")]);
    }

    public function illegalInput_Filter()
    {
        return array([array(
            'filter[illegalparam]' => '5')]);
    }

    public function illegalInput_QueryParameter()
    {
        return array(['illegalparam', 'illegalparam=1']);
    }

}

