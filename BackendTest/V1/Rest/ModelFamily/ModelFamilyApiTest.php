<?php
namespace BackendTest\V1\Rest\ModelFamily;



use BackendTest\lib\BaseObjectTest;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;


class ModelFamilyApiTest extends BaseObjectTest
{
    protected function setUp()
    {
        $this->setPath('/backend/model-family');
        parent::setUp();
    }
//inputs
    public function inputs_SingleItem()
    {
        return array([array(
            'id' => 7)]);
    }
    public function inputs_Filter()
    {

        return array(["field" => (array("id" => 1))],
            ["field" => (array("title" => "ASX"))],
            ["field" => (array("sort" => "3"))],
            ["field" => (array("id"=>1,"title"=>"space star"))],
            ["field" => (array("id"=>2,"sort"=>"8"))],
            ["field" => (array("title"=>'Pajero',"sort"=>"5"))],
            ["field" => (array("id"=>3,"title"=>"Lancer","sort"=>"2"))]
        );
    }
    public function testCreate()
    {
        return array(
            'title' => 'TestConducted',
            'sort' => '10');
    }
    
    public function inputs_UpdateElement()
    {
        return array([array("field" => array("title","sort"), "val" => array('testAbstractUpdate','12'))]);
    }

    public function inputs_Sort()
    {
        return array("sort" => [array(
            "field" => "id", "dir" => "asc", "type" => "num")], [array(
            "field" => "id", "dir" => "desc", "type" => "num")], [array(
            "field" => "title", "dir" => "asc", "type" => "string")], [array(
            "field" => "title", "dir" => "desc", "type" => "string")], [array(
            "field" => "sort", "dir" => "asc", "type" => "string")], [array(
            "field" => "sort", "dir" => "desc", "type" => "string")]);
    }

    public function input_OverviewEmbedded()
    {
        return array(['overview', 'Outlander Metallic-/Perleffektlackierung', '90']);
    }

    public function illegalInput_SingleItem()
    {
        return array(['&', '’%20or%20’1’=’1', '1=1']);
    }

    public function illegalInput_CreateElement()
    {
        return array([array("field" => array("title1","doors"), "val" => array('testUpdate',45))]);
    }
 
    public function illegalInput_UpdateElement()
    {
        return array(['16', array(
            'title1' => 'illtestnew',
            'doors1' => '5')], ['illegal', array(
            'title' => 'illtestnew',
            'doors' => '5')], ['100', array(
            'title' => 'illtestnew',
            'doors' => 89)], ['83', array(
            'title' => 89,
            'doors' => '53')]);
    }

    public function illegalInput_DeleteElement()
    {
        return array(['dfsd', '’%20or%20’1’=’1', '1=1']);
    }

    public function illegalInput_Sort()
    {
        return array([array(
            'sort' => "id=desc1")], [array(
            'sort' => "name=desc1")]);
    }

    public function illegalInput_Filter()
    {
        return array([array(
            'filter[illegalparam]' => '5')]);
    }

    public function illegalInput_QueryParameter()
    {
        return array(['illegalparam', 'illegalparam=1']);
    }
   
}

