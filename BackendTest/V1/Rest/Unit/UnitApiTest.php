<?php
namespace BackendTest\V1\Rest\BodyType;



use BackendTest\lib\BaseObjectTest;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;


class UnitApiTest extends BaseObjectTest
{
    protected function setUp()
    {
        $this->setPath('/backend/unit');
        parent::setUp();
    }
//inputs
    public function inputs_SingleItem()
    {
        return array([array(
            'id' => 2)]);
    }
    public function inputs_Filter()
    {

        return array(["field" => (array("id" => 20))],
            ["field" => (array("textShort" => "EUR"))],
            ["field" => (array("symbol" => "V"))],
            ["field" => (array("textLong"=>'Kilo'))],
            ["field" => (array("symbol"=>'kWh',"textShort"=>"kWh"))],
            ["field" => (array("textShort"=>'PS',"textLong"=>"pferdestärke"))]
        );
    }
    public function testCreate()
    {
        return array(
            'textShort' => 'R',
            'symbol'=>'R',
            'title'=>'R',
            'textLong' => 'Rupee');
    }
    
    public function inputs_UpdateElement()
    {
        return array([array("field" => array("textShort","symbol","textLong","title"), "val" => array('§','R','Rupees','§'))]);
    }

    public function inputs_Sort()
    {
        return array("sort" => [array(
            "field" => "id", "dir" => "asc", "type" => "num")], [array(
            "field" => "id", "dir" => "desc", "type" => "num")], [array(
            "field" => "textShort", "dir" => "asc", "type" => "string")], [array(
            "field" => "textShort", "dir" => "desc", "type" => "string")], [array(
            "field" => "textLong", "dir" => "asc", "type" => "string")], [array(
            "field" => "textLong", "dir" => "desc", "type" => "string")],[array(
            "field" => "symbol", "dir" => "asc", "type" => "string")], [array(
            "field" => "symbol", "dir" => "desc", "type" => "string")]);
    }

    public function input_OverviewEmbedded()
    {
        return array(['overview', 'Outlander Metallic-/Perleffektlackierung', '90']);
    }

    public function illegalInput_SingleItem()
    {
        return array(['&', '’%20or%20’1’=’1', '1=1']);
    }

    public function illegalInput_CreateElement()
    {
        return array([array("field" => array("title1","doors"), "val" => array('testUpdate',45))]);
    }
 
    public function illegalInput_UpdateElement()
    {
        return array(['16', array(
            'title1' => 'illtestnew',
            'doors1' => '5')], ['illegal', array(
            'title' => 'illtestnew',
            'doors' => '5')], ['100', array(
            'title' => 'illtestnew',
            'doors' => 89)], ['83', array(
            'title' => 89,
            'doors' => '53')]);
    }

    public function illegalInput_DeleteElement()
    {
        return array(['dfsd', '’%20or%20’1’=’1', '1=1']);
    }

    public function illegalInput_Sort()
    {
        return array([array(
            'sort' => "id=desc1")], [array(
            'sort' => "name=desc1")]);
    }

    public function illegalInput_Filter()
    {
        return array([array(
            'filter[illegalparam]' => '5')]);
    }

    public function illegalInput_QueryParameter()
    {
        return array(['illegalparam', 'illegalparam=1']);
    }
   
}

