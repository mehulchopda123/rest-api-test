<?php
namespace BackendTest\lib;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use PHPUnit_Framework_TestCase;

abstract  class BaseObjectTest extends PHPUnit_Framework_TestCase
{
    /** @var Client */
    protected $client;
    protected $path;
    protected $headers = [
        'headers' => ['Content-type' => 'application/json', 'Accept' => 'application/json'],
        'http_errors' => false];
    protected $base_uri='http://core.mdm.local:8080';    
    //inputs
    abstract public function inputs_SingleItem();
    abstract public function inputs_Filter();
    abstract public function inputs_UpdateElement();
    abstract public function inputs_Sort();
    abstract public function input_OverviewEmbedded();
    abstract public function illegalInput_SingleItem();
    abstract public function illegalInput_CreateElement();
    abstract public function illegalInput_UpdateElement();
    abstract public function illegalInput_DeleteElement();
    abstract public function illegalInput_Sort();
    abstract public function illegalInput_Filter();
    abstract public function illegalInput_QueryParameter();
    abstract public function testCreate();
//initial Setup
    protected function setUp()
    {
        $this->client = new Client([
            'base_uri' => $this->base_uri
        ], []);
    }
    public function setPath($path)
    {
        $this->path=$path;
    }
    public function getPath(){
        return $this->path;
    }
    public function ThrowsError()
    {
        $this->markTestSkipped('This test was skipped because of the Null Data');
    }

    public function getRequest($path, $paramString)
    {
        switch ($path) {
            case '/': {
                $response = $this->client->get($this->getPath() . '/' . $paramString, $this->headers);
                break;
            }
            case '?': {
                $queryString = http_build_query($paramString);
                $response = $this->client->get($this->path . '?' . $queryString, $this->headers);
                break;
            }
            default: {
                $response = $this->client->get($this->path . $paramString, $this->headers);
            }
        }
        $data = json_decode($response->getBody(), true);
        return $data;
    }

    public function requestedData($data)
    {
        $module = array_keys($data['_embedded']);
        $moduleName = $module[0];//'back-end'
        $data_requested = $data['_embedded'][$moduleName];
        return $data_requested;
    }

    public function checkKeyValue($input, $data)
    {
        foreach ($input as $key => $value) {
            $this->assertArrayHasKey($key, $data);
            $this->assertEquals($value, $data[$key]);
        }
    }

    //Single Item
    /**
     *
     * @dataProvider  inputs_SingleItem
     */
    public function testGet_SingleItem($input)
    {
        $data = $this->getRequest('/', $input['id']);        
        $this->checkKeyValue($input,$data);
        
        $this->assertArrayHasKey('_links', $data);
        $this->assertEquals($this->base_uri . $this->path . '/' . $input['id'], $data['_links']['self']['href']);
    }
    /**
     * @dataProvider  inputs_Filter
     */
    public function testGet_Filter($input)
    {
        if($input==null)
        {
            $this->markTestSkipped('This test was skipped because there is no filter functionality here');                   
        }
        $temp = array();
        foreach ($input as $key => $value) {
            if (is_array($value)) {
                $temp["filter[{$key}]"] = json_encode($value);
            } else {
                $temp["filter[{$key}]"] = $value;
            }
        }
        $queryString = http_build_query($temp);
        $response = $this->client->get($this->path . '?' . $queryString, $this->headers);
        $data = json_decode($response->getBody(), true);
        $data_requested = $this->requestedData($data);
        if ($data_requested != null) {
            //TODO: Do it for a Single ID
            //TODO: For example For id Input will be ('id','','1(no of results)') first check the count it if it ok then run the further test otherwise skipped or break the test
            $first = reset($data_requested);
            $last = end($data_requested);
            $combination = [$first, $last];
            foreach ($input as $key => $val) { //Getting Key Value Pair of all the filter element
                foreach ($combination as $element) {//check for the first,middle and last row fromn the results
                    if (is_numeric($element[$key])) {
                        $a = substr($element[$key], 0, 1);
                        $b = substr($element[$key], -1);
                        var_dump($element[$key]);
                        var_dump($a);
                        var_dump($b);
                        if($val==$element[$key])
                        {
                            $this->assertEquals($val, $element[$key]);
                        }
                        else if ($val == $a) {//This will check if the first letter of output row contains the input element
                            $this->assertEquals($val, $a);
                        }
                        else {//This will check if the last letter of output row contains the input element
                            $this->assertEquals($val, $b);
                        }

                    } else {
                        if (is_array($val))//Check if the Array is passed as the value
                        {
                            foreach ($val as $key1 => $vals) {//Getting {"title":"2016"}
                                $this->assertArrayHasKey($key, $element);
                                $this->assertThat("/{$vals}/i",identicalTo($element[$key][0][$key1]));
                                //$this->assertRegExp("/{$vals}/", $element[$key][0][$key1]);
                            }
                        } else {
                            $this->assertArrayHasKey($key, $element);
                            $this->assertRegExp("/{$val}/i", $element[$key]);

//                            $this->assertRegExp("/{$val}/", $element[$key]);
                        }
                    }
                }
            }
        } else {
            $this->ThrowsError();
        }
    }
// Sorting Test
    /**
     * @dataProvider  inputs_Sort
     */
    public function testGet_Sort($input)
    {
        if($input==null)
        {
            $this->markTestSkipped('This test was skipped because there is no filter functionality here');
        }
        $queryString = "sort={$input['field']}={$input['dir']}";
        $response = $this->client->get($this->path . '?' . $queryString, $this->headers);
        $data = json_decode($response->getBody(), true);
        $data_requested=$this->requestedData($data);//will return data of $data['_embedded]['body_type']
        if ($data_requested != null) {
            $first = reset($data_requested);
            $middleElemeneIndex = ceil(count($data_requested) / 2);
            $middle = $data_requested[$middleElemeneIndex];
            $last = end($data_requested);
            switch ("{$input['type']}&&{$input['dir']}") {
                case "num&&desc":
                    $this->assertEquals(false, $first[$input['field']] < $last[$input['field']]);
                    $this->assertEquals(false, $first[$input['field']] < $middle[$input['field']]);
                    $this->assertEquals(false, $middle[$input['field']] < $last[$input['field']]);
                    break;
                case "string&&asc":
                    $this->assertLessThanOrEqual('0', strcasecmp($first[$input['field']], $last[$input['field']]));
                    $this->assertLessThanOrEqual('0', strcasecmp($first[$input['field']], $middle[$input['field']]));
                    $this->assertLessThanOrEqual('0', strcasecmp($middle[$input['field']], $last[$input['field']]));
                    break;
                case "string&&desc":                
                    $this->assertGreaterThanOrEqual('0', strcasecmp($first[$input['field']], $last[$input['field']]));
                    $this->assertGreaterThanOrEqual('0', strcasecmp($first[$input['field']], $middle[$input['field']]));
                    $this->assertGreaterThanOrEqual('0', strcasecmp($middle[$input['field']], $last[$input['field']]));
                    break;
                default:
                    $this->assertEquals(true, $first[$input['field']] <= $last[$input['field']]);
                    $this->assertEquals(true, $first[$input['field']] <= $middle[$input['field']]);
                    $this->assertEquals(true, $middle[$input['field']] <= $last[$input['field']]);
            }

        } else {
            $this->ThrowsError();
        }
    }



//CRUD Operations

    /**
     * @depends testCreate
     */
    public function testPost_CreateElement($input)
    {
        $queryString = json_encode($input);
        $this->headers['body'] = $queryString;
        $response = $this->client->post($this->path, $this->headers);
        //TODO : check with the input fields.
        $this->assertEquals(201, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);
        $this->checkKeyValue($input, $data);
        return $data['id'];
    }

    /**
     * @depends       testPost_CreateElement
     * @dataProvider  inputs_UpdateElement
     */

    public function testPut_UpdateElement($input, $identifier)
    {
        $temp = array();
        for ($i = 0; $i < count($input['field']); $i++) {
            $temp[$input['field'][$i]] = $input['val'][$i];
        }
        $queryString = json_encode($temp);
        $this->headers['body'] = $queryString;
        $response = $this->client->put($this->path . '/' . $identifier, $this->headers);
        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getBody(), true);
        $input['id'] = $identifier;
        $this->checkKeyValue($temp, $data);
    }
    /**
     * @depends  testPost_CreateElement
     */
    public function testDelete_DeleteElement($id)
    {
        $response = $this->client->delete($this->path . '/' . $id, $this->headers);
        // TODO: To check the deletion restricted the special test case should be written.
        $this->assertEquals(204, $response->getStatusCode());
    }



    /**
     * @dataProvider  illegalInput_SingleItem
     */
    public function testGet_illegalInput_SingleItem($input)
    {
        $response = $this->client->get($this->getPath() . '/' . $input, $this->headers);
        $this->assertEquals(404, $response->getStatusCode());

    }

    /**
     * @dataProvider  illegalInput_CreateElement
     */
    public function testPost_illegalInput_CreateElement($input)
    {
        //TODO: Test case with illegal input(doors with interger, value with different illegal input, sql injection)
        $temp = array();
        for ($i = 0; $i < count($input['field']); $i++) {
            $temp[$input['field'][$i]] = $input['val'][$i];
        }
        $this->headers['body'] = json_encode($temp);
        $response = $this->client->post($this->path, $this->headers);
        $this->assertEquals(422, $response->getStatusCode());
    }

    /**
     * @dataProvider  illegalInput_DeleteElement
     */
    public function testDelete_illegalInput_DeleteElement($input)
    {
        $response = $this->client->delete($this->path . '/' . $input, $this->headers);
        $this->assertEquals(400, $response->getStatusCode());
    }

    /**
     * @dataProvider  illegalInput_UpdateElement
     */
    public function testUpdate_illegalInput_UpdateElement($identifier, $input)
    {
        $this->headers['body'] = json_encode($input);
        $response = $this->client->put($this->path . '/' . $identifier, $this->headers);
        $this->assertEquals(400 or 422, $response->getStatusCode());
    }

    /**
     * @dataProvider  illegalInput_Sort
     */
    public function testGet_illegalInput_Sort($input)
    {
        if($input==null)
        {
            $this->markTestSkipped('This test was skipped because there is no filter functionality here');
        }
        $queryString = http_build_query($input);
        $response = $this->client->get($this->getPath() . '?' . $queryString, $this->headers);
        $this->assertEquals(500, $response->getStatusCode());

    }

    /**
     * @dataProvider  illegalInput_Filter
     */
    public function testGet_illegalInput_Filter($input)
    {
        if($input==null)
        {
            $this->markTestSkipped('This test was skipped because there is no filter functionality here');
        }
        $queryString = http_build_query($input);
        $response = $this->client->get($this->getPath() . '?' . $queryString, $this->headers);
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * @dataProvider  illegalInput_QueryParameter
     */
    public function testGet_illegalInput_QueryParameter($input)
    {
        $data = $this->getRequest('','');
        $response = $this->client->get($this->path . '?' . $input, $this->headers);
        $data_query= json_decode($response->getBody(), true);
        $this->assertSameSize($data, $data_query);
    }

//TODO: Test cases For Overview and Embedded call.
//TODO: in one page(Not paged), check with title or possible property(Simply write one test case which can be also valid for other properties.)

    /**
     * @dataProvider  input_OverviewEmbedded
     */
    public function testGet_OverviewEmbedded($input,$title,$id)
    {
        $response = $this->client->get($this->path . '?' . $input, $this->headers);
        $data = json_decode($response->getBody(), true);
        if ($data != null){
        $this->assertArrayHasKey('self', $data['_links']);
        $this->assertEquals($this->base_uri . $this->path . '?' . $input . '=', $data['_links']['self']['href']);
        $this->assertArrayNotHasKey('first', $data['_links']);
        $this->assertArrayNotHasKey('last', $data['_links']);
        $this->assertArrayNotHasKey('next', $data['_links']);
        $request_data = $this->requestedData($data);
        $first = reset($request_data);//it will return data in form of $data['_embedded']['body_type]
        $this->assertArrayHasKey('title',$first);
        $this->assertArrayHasKey('id',$first);
        $this->assertNotEmpty($first['title']);
        $this->assertNotEmpty($first['id']);
        }
    }

}

