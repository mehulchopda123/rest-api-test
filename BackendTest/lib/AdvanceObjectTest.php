<?php

namespace BackendTest\lib;

abstract class AdvanceObjectTest extends BaseObjectTest
{
    //Single Item
    /**
     *
     * @dataProvider  inputs_SingleItem
     */
    public function testGet_SingleItem($input)
    {
        $data = $this->getRequest('/', $input['id']);
        for ($i = 0; $i < count($input['required']); $i++) {
            //Just check the required field and also check that they are not empty
            $this->assertArrayHasKey($input['required'][$i], $data);
            $this->assertNotEmpty($data[$input['required'][$i]]);
            if ($input['required'][$i] == 'price') {
                $count = count($data[$input['required'][$i]]['prices']);
                $this->assertEquals(1, $count);
            }
            if ($input['required'][$i] == 'modelYears') {
                $count = count($data[$input['required'][$i]]);
                $this->assertGreaterThanOrEqual(1, $count);
            }
        }
        $this->assertArrayHasKey('_links', $data);
        $this->assertEquals($this->base_uri . $this->path . '/' . $input['id'], $data['_links']['self']['href']);
    }
// Sorting Test
    /**
     * @dataProvider  inputs_Sort
     */
    public function testGet_Sort($input)
    {
        $queryString = "sort={$input['field']}={$input['dir']}";
        var_dump($queryString);
        $response = $this->client->get($this->path . '?' . $queryString, $this->headers);
        $data = json_decode($response->getBody(), true);
        $data_requested = $this->requestedData($data);//will return data of $data['_embedded]['body_type']
        if ($data_requested != null) {
            //only take the first , middle and last element from the retrieved data and compare them
            $first = reset($data_requested);
            $middleElementIndex = ceil(count($data_requested) / 2);
            $middle = $data_requested[$middleElementIndex];
            $last = end($data_requested);
            $parts = explode('.', $input['field']);//Divide Input to get the key value pair so the $parts will look like('rim','modelYears','title') from sort=rim.modelYears.title
            $parts_first = reset($parts);
            $parts_last = end($parts);
            $input['field'] = array_slice($parts, -2, 1)[0];//get Second last element i.e modelYears which we doing sorting on
            if ($input['field'] == $parts_first) {//if sort=title.desc
                $fetch_array_first = $first;
                $fetch_array_middle = $middle;
                $fetch_array_last = $last;
            } else {//for sort=modelYears.title or sort=rim.modelyears.title
                $fetch_array_first = $first[$parts_first];
                $fetch_array_middle = $middle[$parts_first];
                $fetch_array_last = $last[$parts_first];
            }
            switch ("{$input['type']}&&{$input['dir']}") {
                case "num&&desc":
                    $this->assertEquals(false, $first[$input['field']] < $last[$input['field']]);
                    $this->assertEquals(false, $first[$input['field']] < $middle[$input['field']]);
                    $this->assertEquals(false, $middle[$input['field']] < $last[$input['field']]);
                    break;
                case "string&&asc":
                    if (is_array($fetch_array_first)) {
                        for ($i = 0; $i < count($parts); $i++) {  //Do looping until number of parts
                            $x = array_keys($fetch_array_first);  //get all the keys and match it with the input(i.e modelYears)
                            $m = array_key_exists($input['field'], $x);
                            if ($input['field'] != $parts_first && $m)//for more than 3 level
                            {
                                $fetch_array_first = $fetch_array_first[$parts[$i + 1]];//for sort=X.rim.modelYears.title  in case for the future
                                $fetch_array_middle = $fetch_array_middle[$parts[$i + 1]];
                                $fetch_array_last = $fetch_array_last[$parts[$i + 1]];
                            }
                            else {
                                //For rim.modelYears.title (3 level)
                                $first_ele = $fetch_array_first[$input['field']];
                                $middle_ele = $fetch_array_middle[$input['field']];
                                $last_ele = $fetch_array_last[$input['field']];
                                foreach ($first_ele as $a => $b) {//Check the Array is multidimensional
                                    if (!is_int($a)) {
                                        $this->assertLessThanOrEqual('0', strcasecmp($first_ele[$parts_last], $last_ele[$parts_last]));
                                        $this->assertLessThanOrEqual('0', strcasecmp($first_ele[$parts_last], $middle_ele[$parts_last]));
                                        $this->assertLessThanOrEqual('0', strcasecmp($middle_ele[$parts_last], $last_ele[$parts_last]));
                                        break;
                                    }
                                    else{
                                        $this->assertLessThanOrEqual('0', strcasecmp($first_ele[0][$parts_last], $last_ele[0][$parts_last]));
                                        $this->assertLessThanOrEqual('0', strcasecmp($first_ele[0][$parts_last], $middle_ele[0][$parts_last]));
                                        $this->assertLessThanOrEqual('0', strcasecmp($middle_ele[0][$parts_last], $last_ele[0][$parts_last]));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else { //for simple element like sort=title=desc
                        $this->assertLessThanOrEqual('0', strcasecmp($first[$input['field']], $last[$input['field']]));
                        $this->assertLessThanOrEqual('0', strcasecmp($first[$input['field']], $middle[$input['field']]));
                        $this->assertLessThanOrEqual('0', strcasecmp($middle[$input['field']], $last[$input['field']]));
                    }
                    break;
                case "string&&desc":
                    if (is_array($fetch_array_first)) {
                        for ($i = 0; $i < count($parts); $i++) {
                            $x = array_keys($fetch_array_first);
                            $m = array_key_exists($input['field'], $x);
                            if ($input['field'] != $parts_first && $m)//for more than 3 level
                            {
                                $fetch_array_first = $fetch_array_first[$parts[$i + 1]];
                                $fetch_array_middle = $fetch_array_middle[$parts[$i + 1]];
                                $fetch_array_last = $fetch_array_last[$parts[$i + 1]];
                            } else {//For rim.modelYears.title (3 level)
                                $first_ele = $fetch_array_first[$input['field']];
                                $middle_ele = $fetch_array_middle[$input['field']];
                                $last_ele = $fetch_array_last[$input['field']];
                                foreach ($first_ele as $a => $b) {
                                    if (!is_int($a)) {
                                        $this->assertGreaterThanOrEqual('0', strcasecmp($first_ele[$parts_last], $last_ele[$parts_last]));
                                        $this->assertGreaterThanOrEqual('0', strcasecmp($first_ele[$parts_last], $middle_ele[$parts_last]));
                                        $this->assertGreaterThanOrEqual('0', strcasecmp($middle_ele[$parts_last], $last_ele[$parts_last]));
                                        break;
                                    }
                                    else{
                                        $this->assertGreaterThanOrEqual('0', strcasecmp($first_ele[0][$parts_last], $last_ele[0][$parts_last]));
                                        $this->assertGreaterThanOrEqual('0', strcasecmp($first_ele[0][$parts_last], $middle_ele[0][$parts_last]));
                                        $this->assertGreaterThanOrEqual('0', strcasecmp($middle_ele[0][$parts_last], $last_ele[0][$parts_last]));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        $this->assertGreaterThanOrEqual('0', strcmp($first[$input['field']], $last[$input['field']]));
                        $this->assertGreaterThanOrEqual('0', strcmp($first[$input['field']], $middle[$input['field']]));
                        $this->assertGreaterThanOrEqual('0', strcmp($middle[$input['field']], $last[$input['field']]));
                    }
                    break;
                default:
                    $this->assertEquals(true, $first[$input['field']] < $last[$input['field']]);
                    $this->assertEquals(true, $first[$input['field']] < $middle[$input['field']]);
                    $this->assertEquals(true, $middle[$input['field']] < $last[$input['field']]);
            }

        } else {
            $this->ThrowsError();
        }
    }

    /**
     * @dataProvider  inputs_Filter
     */
    public function testGet_Filter($input)
    {
        $temp = array();
        foreach ($input as $key => $value) {
            if (is_array($value)) {  //For String Like filter[rim]={'modelYears'=>{'title':'2015'}
                $temp["filter[{$key}]"] = json_encode($value);
            } else {
                $temp["filter[{$key}]"] = $value;//For Simple String filter[id]=25
            }
        }
        $queryString = http_build_query($temp);
        $response = $this->client->get($this->path . '?' . $queryString, $this->headers);
        $data = json_decode($response->getBody(), true);
        $data_requested = $this->requestedData($data);
        if ($data_requested != null) {
            //TODO: Do it for a Single ID
            //TODO: For example For id Input will be ('id','','1(no of results)') first check the count it if it ok then run the further test otherwise skipped or break the test
            //Just Check For the First and Last Element
            $first = reset($data_requested);
            $last = end($data_requested);
            $combination = [$first,$last];//Array of first and last element of retrieved data
            foreach ($input as $key => $val) { //Getting Key Value Pair of all the filter element
                foreach ($combination as $element) {
                    if (is_numeric($element[$key])) {//to diff. between int(i.e id) and String (Other except id)
                        $a = substr($element[$key], 0, 1);//
                        $b = substr($element[$key], -1);
                        if ($val == $a) {
                            $this->assertEquals($val, $a);
                        } else {
                            $this->assertEquals($val, $b);
                        }

                    } else {
                        if (is_array($val))//Check if the Array is passed as the value
                        {
                            foreach ($val as $key1 => $vals) {//Getting {"title":"2016"} or {"modelYears":{"title":"2015"}}
                                if (is_array($vals)) {
                                    foreach ($val as $key_sub => $vals_sub) {
                                        if (!is_int($key_sub)) {
                                            $this->assertRegExp("/{$vals_sub['title']}/", $element[$key][$key1]['title']);
                                        }
                                        else{
                                            $this->assertRegExp("/{$vals_sub['title']}/", $element[$key][$key1][0]['title']);
                                        }
                                    }

                                } else {
                                    //Check whether Array is single or multidimensional
                                    foreach ($element[$key] as $a => $b) {
                                        if (!is_int($a)) {//not multidimensional
                                            var_dump($element[$key]);
                                            $this->assertArrayHasKey($key, $element);
                                            $this->assertRegExp("/{$vals}/", $element[$key][$key1]);
                                            break;
                                        }
                                        else{//multidimensional
                                            $this->assertArrayHasKey($key, $element);
                                            $this->assertRegExp("/{$vals}/", $element[$key][0][$key1]);
                                            break;
                                        }
                                    }                                    
                                }
                            }
                        } else {
                            $this->assertArrayHasKey($key, $element);
                            $this->assertRegExp("/{$val}/", $element[$key]);
                        }
                    }
                }
            }
        } else {
            $this->ThrowsError();
        }
    }
}